<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Student;

use Session;

class StudentController extends Controller
{
    public function ShowAddMeal()
    {
        return view('addmeal');
    }

    public function UpdateAddMeal(Request $request)
    {
        $student = Student::findOrFail(Session::get('user_id'));

        $new_meal_number = $student->meal + $request->meal_number;

        $student->update([
            'meal' => $new_meal_number 
        ]);

        return redirect()->route('home');
    }

    public function MemberEdit($id)
    {
        $student = Student::findOrFail($id);
        //dd($student);

        return view('user_member_edit' , compact('student'));
    }

    public function UploadImage($id , Request $request)
    {

        $this->validate($request, [
                'filename' => 'required',
               // 'filename.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
      
        if($request->hasfile('filename'))
         {

            foreach($request->file('filename') as $image)
            {
                $name=$image->getClientOriginalName();
                // $exten= $image->getClientOriginalExtension();
               $image->move(public_path().'/images/', $name);//image copy to the public folder
               
                $data = $name;  
                $form= new Student();
              //  $form->filename=json_encode($data);
                


                 $student = Student::findOrFail($id);

        $student->update(
            [
                'image' => $data
            ] );
                
             //   $form->save();
            }
         }
         
         return back()->with('success', 'Your images has been successfully');

    }

     public function UserMemberUpdate($id , Request $request)
    {
        $student = Student::findOrFail($id);

        $student->update(
            [
                'email' => $request->email,
                'phone' => $request->phone
            ]
        );

      
       

       return redirect()->route('detail.pdf');
    }

    public function ShowStudentInfo()
    {
        $student = Student::findOrFail(Session::get('user_id'));

        return view('profile_show' , compact('student'));
    }

    public function ProfileUpdate()

    {
       // $student = Student::findorFail(Session::get('user_id'));
        $student = Student::findOrFail(Session::get('user_id'));
        return view('profile_update' , compact('student'));
    }
    public function ProfileView()
    {
        $student = Student::findOrFail(Session::get('user_id'));
        return view('profile_view',compact('student'));
    }
    public function UpdateDone($id , Request $request)
    {
        $student = Student::findOrFail($id);

        $student->update(
            [
                'email' => $request->email,
                'phone' => $request->phone
            ]
        );

        return redirect()->route('viewprofile');
    }
    
}
