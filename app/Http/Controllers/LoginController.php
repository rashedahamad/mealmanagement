<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

use Session;

class LoginController extends Controller
{
    public function signin(Request $request)
    {

    	//validate

    	$request->validate([

    		'user_name' => 'required',
    		'password' => 'required'


    	]);

    	//login

    	$student = Student::where('user_name'  , $request->user_name)->where('password' , $request->password)
					->first();



		if($student)
			{
				Session::put('user_id' , $student->id);
				Session::put('user_role' , $student->role);
				Session::put('user_name' , $student->user_name);

				if($student->role == 1)
				{
					return redirect()->route('admin.home');
				}

				return redirect()->route('home');
			}
			else
			{
				return redirect()->route('login');
			}
		}
		
		public function signout()
		{
			  Session::put('user_id' , null );
				Session::put('user_name' , null );
				Session::put('user_role' , null );

				return redirect()->route('login');
		}
}
