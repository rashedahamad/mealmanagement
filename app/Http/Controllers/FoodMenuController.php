<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\FoodMenu;

class FoodMenuController extends Controller
{
    public function index()
    {
        $foodmenus =  FoodMenu::all();


    	return view('foodmenu' , compact('foodmenus'));
    }


}
