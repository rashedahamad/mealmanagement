<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Session;

class HomeController extends Controller
{
    public function index()
    {
    	return view('home');
    }

    public function ShowMealRate()
    {

        $total_amount = DB::table('students')->sum('amount');

        $total_meal = DB::table('students')->sum('meal');

        if($total_meal == 0)
            $meal_rate = 0;
        else
        $meal_rate = round($total_amount / $total_meal) ;

        //dd($meal_rate);

        return view('mealrate' , compact('meal_rate'));
    }

    public function ShowTransaction()
    {
        $total_amount = DB::table('students')->sum('amount');

        $total_meal = DB::table('students')->sum('meal');

        if($total_meal == 0)
            $meal_rate = 0;
        else
        $meal_rate = round($total_amount / $total_meal) ;

        

        $personal_meal_number = DB::table('students')->where('id' , Session::get('user_id'))->value('meal');

        $personal_meal_cost = $meal_rate * $personal_meal_number ;
       
        $personal_amount = DB::table('students')->where('id' , Session::get('user_id'))->value('amount');

        $money = round($personal_amount - $personal_meal_cost) ;

        $compact = compact('money' , 'personal_meal_cost');

        //dd($personal_meal_cost);

        return view('transaction' , $compact);
    }
    
}
