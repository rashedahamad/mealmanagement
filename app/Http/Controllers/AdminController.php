<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\FoodMenu;

use App\Student;

use DB;

use Session;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin-home');
    }

    public function foodmenu()
    {
        $foodmenus =  FoodMenu::all();
        //$student = Student::all();
        return view('admin-foodmenu',compact('foodmenus'));
    }

    public function FoodEdit($id)
    {
        $foods = FoodMenu::findOrFail($id);

        
       return view('admin-changefood',compact('foods'));
    }

    public function UpdateFood($id , Request $request)
    {
        $foods = FoodMenu::findOrFail($id);

        $foods->update(
            [
                'item1' => $request->item1,
                'item2' => $request->item2
            ]
        );

        return redirect()->route('admin.foodmenu');
    }

    public function ShowMealRate()
    {

        $total_amount = DB::table('students')->sum('amount');

        $total_meal = DB::table('students')->sum('meal');

        if($total_meal == 0)
            $meal_rate = 0;
        else
        $meal_rate = round($total_amount / $total_meal) ;

        //dd($meal_rate);

        return view('admin-mealrate' , compact('meal_rate'));
    }

    public function ShowAdminInfo()
    {
        $student = Student::findOrFail(Session::get('user_id'));

        return view('admin-profile' , compact('student'));
    }

    public function MemberModification()
    {
        $students = Student::where('role' , 0)->get();

        $moneys = array();

        foreach($students as $student)
        {
            $total_amount = DB::table('students')->sum('amount');

            $total_meal = DB::table('students')->sum('meal');

            if($total_meal == 0)
                $meal_rate = 0;
            else
            $meal_rate = round($total_amount / $total_meal) ;

            

            $personal_meal_number = DB::table('students')->where('id' , $student->id )->value('meal');

            $personal_meal_cost = $meal_rate * $personal_meal_number ;
        
            $personal_amount = DB::table('students')->where('id' , $student->id )->value('amount');

            $money = round($personal_amount - $personal_meal_cost) ;

            $moneys[$student->id] = $money ;
        }


        return view('all-user' , compact('students' , 'moneys'));
    }

    public function Reset()
    {
        return view('confirm-reset');
    }

    public function ConfirmReset()
    {

        DB::update('UPDATE students SET amount = 0 , meal= 0 ');

        return redirect()->route('admin.home');
    }

    public function ShowAddAmountForm()
    {
            return view('addamount');
    }

    public function AddAmount(Request $request)
    {
        $student = Student::where('email' , $request->member_email)->first();

        $student->update([

            'amount' => $student->amount + $request->amount

        ]);

        return redirect()->route('admin.home');
    }

    public function MemberEdit($id)
    {
        $student = Student::findOrFail($id);

        return view('member-edit' , compact('student'));
    }

    

    public function UpdateMember($id , Request $request)
    {
        $student = Student::findOrFail($id);

        $student->update(
            [
                'email' => $request->email,
                'phone' => $request->phone
            ]
        );

        return redirect()->route('member.modification');
    }

    public function MemberDelete($id)
    {
        $student = Student::findOrFail($id);

        $student->delete();

        return redirect()->route('member.modification');
    }
}
