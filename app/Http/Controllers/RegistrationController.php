<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

use Session;

class RegistrationController extends Controller
{
    public function index()
    {
    	return view('register');
    }

    public function create(Request $request)
    {
    	//Validate

    	$request->validate([

    		'user_name' => 'required' ,
    		 'first_name' => 'required',
    		 'last_name' => 'required',
             'email' => 'required | unique:students',
             'phone' => 'required',
             'gender' => 'required',
             'password' => 'required|confirmed',
			 'password_confirmation' => 'required' , 
			 'date' => 'required',
			 'month' => 'required',
			 'year' => 'required',


    	]);

    	$date_time = $request->date . "/" . $request->month . "/" . $request->year;


    	//Store

    	 $student = Student::create([

    	 	'user_name' => $request->user_name , 
    	 	'first_name' => $request->first_name , 
    	 	'last_name' => $request->last_name , 
    	 	'dob' => $date_time,
    	 	'phone' => $request->phone,
    	 	'email' => $request->email,
    	 	'gender' => $request->gender,
    	 	'password' => $request->password

    	 ]);


    	if($student)
    	{
    		Session::put('is_register' , true);

    		return redirect()->route('login');
    	} 

    }
}
