<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use DB;
use Session;
use PDF;

class DynamicPDFController extends Controller
{

    function pdf() {

     $customer_data = $this->get_customer_data();
     

     // $total_amount = DB::table('students')->sum('amount');

     //    $total_meal = DB::table('students')->sum('meal');

     //    if($total_meal == 0)
     //        $meal_rate = 0;
     //    else
     //    $meal_rate = round($total_amount / $total_meal) ;

        

     //    $personal_meal_number = DB::table('students')->where('id' , Session::get('user_id'))->value('meal');

     //    $personal_meal_cost = $meal_rate * $personal_meal_number ;
       
     //    $personal_amount = DB::table('students')->where('id' , Session::get('user_id'))->value('amount');

     //    $money = round($personal_amount - $personal_meal_cost) ;

     //    $compact = compact('money' , 'personal_meal_cost');

        //dd($compact);

       // Session::put('money' , $money);



        $data['customer_data']= $customer_data;
        // dd($data);
        $pdf = PDF::loadView('pdf.document', $data);
        

        return $pdf->stream('document.pdf',compact('customer_data'));
    }

    function index()
    {
     $customer_data = $this->get_customer_data();

     $total_amount = DB::table('students')->sum('amount');

        $total_meal = DB::table('students')->sum('meal');

        if($total_meal == 0)
            $meal_rate = 0;
        else
        $meal_rate = round($total_amount / $total_meal) ;

        

        $personal_meal_number = DB::table('students')->where('id' , Session::get('user_id'))->value('meal');

        $personal_meal_cost = $meal_rate * $personal_meal_number ;
       
        $personal_amount = DB::table('students')->where('id' , Session::get('user_id'))->value('amount');

        $money = round($personal_amount - $personal_meal_cost) ;

        $compact = compact('money' , 'personal_meal_cost');

        //dd($compact);

        Session::put('money' , $money);
      //  Session::put('');

     return view('dynamic_pdf')->with('customer_data', $customer_data);
    }

    function get_customer_data()
    {
    	$customer_data = DB::table('students')->where('id' , Session::get('user_id'))
         ->limit(10)
         ->get();
    	//$customer_data = Student::findOrFail(Session::get('user_id'));

     return $customer_data;
    }

   

    function pdfd()
    {
     // $pdf = \App::make('dompdf.wrapper');
     // $pdf->loadHTML($this->convert_customer_data_to_html());
     // return $pdf->stream();
    }

 



     function convert_customer_data_to_html()
    {

    	$money = Session::get('money');
     $customer_data = $this->get_customer_data();



     $output = '
     <h3 align="center">Customer Data</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
    <th style="border: 1px solid; padding:12px;" width="15%">Name</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Email</th>
    <th style="border: 1px solid; padding:12px;" width="15%">Phone</th>
    <th style="border: 1px solid; padding:12px;" width="15%">Meal</th>
    <th style="border: 1px solid; padding:12px;" width="15%">Amount</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Transaction</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Image</th>
   </tr>
     ';  




     foreach($customer_data as $customer)
     {
        //<center><img src="{{asset('/images/'.$customer->image)}}" width="70px" height="70px" alt="14"/></center>
        $img = asset('/images/'.$customer->image);
        // $img1 = "/public/images/$customer->image";
        //dd($img);

      $output .= '
      <tr>
       <td style="border: 1px solid; padding:12px;">'.$customer->user_name.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->email.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->phone.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->meal.'</td>
       <td style="border: 1px solid; padding:12px;">'.$customer->amount.'</td>
       <td style="border: 1px solid; padding:12px;">';$output .='<img src='.$img.' width="70px" height="70px" alt="14"/></td>';
       


      // <td style="border: 1px solid; padding:12px;">'.$money*(-1).'</td>'

     //$output. = '<img src=$img1/>';

       
        if($money > 0)
     {
       $output .= '<td style="border: 1px solid; padding:12px;">Your will get '. $money. 'tk</td>';
     }



      elseif($money < 0)
     {
       $output .= '<td style="border: 1px solid; padding:12px;">You have to pay '. $money*(-1). 'tk</td>';
     }

     else
     {
     	$output .= '<td style="border: 1px solid; padding:12px;">Your transaction have  been complete.</td>';

     }


     
       	
      
      
    $output .='</tr>';
      
     }
     $output .= '</table>';
     return $output;
    }
}