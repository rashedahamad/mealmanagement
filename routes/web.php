<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
})->name('login');

//Website

Route::get('/registration' , 'RegistrationController@index')->name('registration');
Route::post('/user/create' , 'RegistrationController@create')->name('user.create');

//

Route::post('/user/signin','LoginController@signin')->name('user.signin');
Route::get('/user/signout','LoginController@signout')->name('user.signout');



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/foodmenu', 'FoodMenuController@index')->name('foodmenu');

Route::get('/mealrate', 'HomeController@ShowMealRate')->name('mealrate');

Route::get('/transaction', 'HomeController@ShowTransaction')->name('transaction');

Route::get('/addmeal', 'StudentController@ShowAddMeal')->name('addmeal');
Route::post('/update/addmeal', 'StudentController@UpdateAddMeal')->name('update.addmeal');

Route::get('/showprofile', 'StudentController@ShowStudentInfo')->name('showprofile');


Route::get('/update/profile', 'StudentController@ProfileUpdate')->name('updateprofile');
Route::post('/updateprofile/{id}', 'StudentController@UpdateDone')->name('updatedone');
Route::get('/viewprofile', 'StudentController@ProfileView')->name('viewprofile');
//Route::post('/update/addmeal', 'StudentController@UpdateAddMeal')->name('update.addmeal');


Route::get('/dynamic_pdf', 'DynamicPDFController@index')->name('detail.pdf');

Route::get('/dynamic_pdf/pdf', 'DynamicPDFController@pdf');

Route::get('user/memberedit/{id}', 'StudentController@MemberEdit')->name('user.memberedit');
Route::post('user/memberupdate/{id}','StudentController@UserMemberUpdate')->name('update.user_member');

Route::post('user/imageupload/{id}', 'StudentController@UploadImage')->name('upload.image');


//Admin

Route::get('/admin-home', 'AdminController@index')->name('admin.home');

Route::get('/admin-modification', 'AdminController@MemberModification')->name('member.modification');

Route::get('/admin-reset', 'AdminController@Reset')->name('reset');

Route::post('/admin-reset-confirm', 'AdminController@ConfirmReset')->name('confirm.reset');

Route::get('/admin-showaddamount', 'AdminController@ShowAddAmountForm')->name('showaddamount');

Route::post('/admin-addamount', 'AdminController@AddAmount')->name('addamount');

Route::get('/admin-foodmenu', 'AdminController@foodmenu')->name('admin.foodmenu');

Route::get('/admin-mealrate', 'AdminController@ShowMealRate')->name('admin.mealrate');

Route::get('/admin-showprofile', 'AdminController@ShowAdminInfo')->name('admin.showprofile');

Route::get('/admin-memberedit/{id}', 'AdminController@MemberEdit')->name('admin.memberedit');

Route::post('/admin-updatemember/{id}', 'AdminController@UpdateMember')->name('update.member');

Route::get('/admin-memberdelete/{id}', 'AdminController@MemberDelete')->name('admin.memberdelete');


Route::get('/admin-editfood/{id}', 'AdminController@FoodEdit')->name('admin.editfood');

Route::post('/admin-changefood/{id}', 'AdminController@UpdateFood')->name('update.changefood');


Route::get('/admin-showprofile', 'LiveSearch@index')->name('admin.showprofile');
Route::get('/live_search/action', 'LiveSearch@action')->name('live_search.profileview');