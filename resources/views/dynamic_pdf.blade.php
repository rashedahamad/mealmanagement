@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
 <head>
  <title>Laravel - How to Generate Dynamic PDF from HTML using DomPDF</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
   }
  </style>
 </head>
 <body>
  <br />
  <div class="container">
   <h3 align="center">Your Profile</h3><br />
   
   <div class="row">
    <div class="col-md-7" align="right">
     <h4>Customer details</h4>
    </div>
    <div class="col-md-5" align="right">
     <a href="{{ url('dynamic_pdf/pdf') }}" class="btn btn-danger">Convert into PDF</a>
    </div>
   </div>
   <br />
   <div class="table-responsive">
    <table class="table table-striped table-bordered">
     <thead>
      <tr>
       <th>Name</th>
       <th>Email</th>
       <th>Phone</th>
       <th>Meal</th>
       <th>Amount</th>
       <th>Transaction</th>
       <th>Profile Picture</th>
       <th>Option</th>
      </tr>
     </thead>
     <tbody>
     @foreach($customer_data as $customer)

     <?php $test = Session::get('money')  ?>
    
      <tr>
       <td>{{ $customer->user_name }}</td>
       <td>{{ $customer->email }}</td>
       <td>{{ $customer->phone }}</td>
       <td>{{ $customer->meal }}</td>
       <td>{{ $customer->amount }}</td>
        <td>@if($test < 0)

        <h5>You have to pay {{ Session::get('money')*(-1) }} tk</h5>

        
        @elseif($test > 0)

        <h5> You will get {{ Session::get('money') }} tk</h5>

        @else

        <h5>Your transaction is completed</h5>

        @endif
      </td>
     
        <td><center><img src="{{url('/public/images/'.$customer->image)}}" width="70px" height="70px" alt="14"/></center></td>
        <td> <a href="{{route('user.memberedit',$customer->id)}}" type="submit" class="btn btn-primary">Edit</a></td>
     
      </tr>
     @endforeach
     </tbody>
    </table>
   </div>
  </div>
 </body>
</html>
@endsection