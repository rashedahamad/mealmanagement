@extends('layouts.admin-app')

@section('content')

<div class="container">

<form action="{{ route('addamount') }}" method="post">

{{ csrf_field() }}
  <div class="form-group">
    <label for="exampleInputEmail1">Member Email</label>
    <input type="text" name="member_email" class="form-control"  placeholder="Enter Member email">
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">Amount</label>
    <input type="text" name="amount" class="form-control"  placeholder="Enter Amount">
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

</div>

@endsection