<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Styles -->
        <style>
            #maindiv{
                  background-image: url({{asset('public/image/20.jpg')}}); /* The image used */
                  background-color: #cccccc; /* Used if the image is unavailable */
                  height: 700px; /* You must set a specified height */
                  background-position: center; /* Center the image */
                  background-repeat: no-repeat; /* Do not repeat the image */
                  background-size: cover;
            }
            
            </style>
    </head>
   <body >

    @if(Session::get('is_register'))
        <div class="container">
            You are just registered
            {{ Session::put('is_register' , false) }}
        </div>
    @endif

    <form action="{{ route('user.signin') }}" method="post" >
        {{ csrf_field() }}
        <center><div id="maindiv">
            <br><br><h1 style="color:black">Login</h1>
           <h1 style="color:black;"><pre> User name/Email<br> <input type="text" name="user_name" value="{{old('user_name')}}"  /></h1><span> @if($errors) {{$errors->first('user_name') }} @endif</span></pre>  
             <h1 style="color:black;"><pre> Password <br> <input type="Password" name="password" value="{{old('password')}}"  /></h1><span> @if($errors) {{$errors->first('password') }} @endif</span></pre><br>
                        
                        <input  type="submit"  value="Login"  ><br>
                        <h3><a href="{{ route('registration') }}">Register an account</a></h3>
                        
                        <h3><a href="">Forgot Password ?</a></h3>
                        


            
        </div></center>
        
        
    </form>


</body>
</html>
