@extends('layouts.MemberProfileModification')



@section('content')


<div class="container">

<form method="post" action="{{ route('updatedone' , $student->id) }}">

{{ csrf_field() }}

  <div class="form-group">
    <label for="exampleInputEmail1">User Email</label>
    <input type="text"  class="form-control" name="email" value="{{ $student->email }}">  
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">Phone No</label>
    <input type="text"  class="form-control" name="phone" value="{{ $student->phone }}">  
  </div>

  
  <button class="btn btn-primary"> Confirm Edit </button>

</form>


</div>

@endsection