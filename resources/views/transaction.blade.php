@extends('layouts.app')

@section('content')

<div class="container">

    <center>
        
        @if($money < 0)

        <h3>Your meal cost is {{ $personal_meal_cost }} and You have to pay {{$money*(-1)}} tk</h3>

        
        @elseif($money > 0)

        <h3>Your meal cost is {{ $personal_meal_cost }} and You will get {{$money}} tk</h3>

        @else

        <h3>Your meal cost is {{ $personal_meal_cost }} and Your transaction is completed</h3>

        @endif
        
    </center>

</div>

@endsection