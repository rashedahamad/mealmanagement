@extends('layouts.app')

@section('content')

<head>
 <!-- <title>Select a image file</title>-->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<form method="post" action="{{ route('update.user_member' , $student->id) }}" enctype="multipart/form-data">

{{ csrf_field() }}

  <div class="form-group">
    <label for="exampleInputEmail1">User Email</label>
    <input type="text"  class="form-control" name="email" value="{{ $student->email }}">  
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">Phone No</label>
    <input type="text"  class="form-control" name="phone" value="{{ $student->phone }}">  
  </div>

  <button class="btn btn-primary"> Confirm Edit  </button>
</form>
</div>
  <div class="container">

<form method="post" action="{{ route('upload.image' , $student->id) }}" enctype="multipart/form-data">
@csrf

  <div class="container">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

        @if(session('success'))
        <div class="alert alert-success">
          {{ session('success') }}
        </div> 
        @endif

   <!-- <h3 class="jumbotron">Laravel Multiple File Upload</h3>-->
 
   		 <label for="exampleInputEmail1">Upload Your Photo</label>

        <div class="input-group control-group increment" >
          <input type="file" name="filename[]" class="form-control">
          <div class="input-group-btn"> 
            <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
          </div>
        </div>
        <div class="clone hide">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="file" name="filename[]" class="form-control">
            <div class="input-group-btn"> 
              <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
            </div>
          </div>
        </div><br>
       

       <center><button class="btn btn-primary"> Upload Photo  </button></center> 

      </form>  

       

     
  </div>


<script type="text/javascript">


    $(document).ready(function() {

      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });

    });

</script>
</body>



</div>

@endsection
