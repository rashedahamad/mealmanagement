@extends('layouts.app')

@section('content')


<div class="container">

<form method = "POST" action="{{ route('update.addmeal') }}">
    {{ csrf_field() }}

  <div class="form-group">
    <label for="exampleFormControlFile1">Select Meal number</label>
    <select name="meal_number" class="form-control form-control">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
    </select>
  </div>
  <button type="submit" class="btn btn-primary mb-2">Confirm Meal</button>
</form>


</div>

@endsection