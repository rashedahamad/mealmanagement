@extends('layouts.admin-app')

@section('content')

<div class="container">

<form action="{{ route('update.changefood' , $foods->id  ) }}" method="post">

{{ csrf_field() }}
  <div class="form-group">
    <label for="exampleInputEmail1">Item1</label>
    <input type="text" name="item1" class="form-control"  value="{{ $foods->item1 }}">
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">Item2</label>
    <input type="text" name="item2" class="form-control"  value="{{ $foods->item2 }}">
  </div>
  
  <button type="submit" class="btn btn-primary">Change</button>
</form>

</div>

@endsection