@extends('layouts.admin-app')

@section('content')

<div class="container">

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">User Name</th>
      <th scope="col">Email</th>
      <th scope="col">Phone no</th>
      <th scope="col">Amount</th>
      <th scope="col">Meal</th>
      <th scope="col">Transaction Status</th>
      <th scope="col">Option</th>
    </tr>
  </thead>
  <tbody>
    @foreach($students as $student)
        <tr>
            <td> {{ $student -> id }} </td>
            <td> {{ $student -> user_name }} </td>
            <td> {{ $student -> email }} </td>
            <td> {{ $student -> phone }} </td>
            <td> {{ $student -> amount }} </td>
            <td> {{ $student -> meal }} </td>
            <td> 
            
            @if($moneys[$student->id] > 0)
            Member will get  {{$moneys[$student->id]}}
                
            @elseif($moneys[$student->id] < 0)
            Member have to pay {{ $moneys[$student->id] }}
                
            @else
                Memeber's All Transaction cleared
            @endif
            
            </td>
            <td>
                <a href="{{ route('admin.memberedit' , $student->id ) }}" type="submit" class="btn btn-primary">Edit</a>

                <a href="{{ route('admin.memberdelete' , $student->id ) }}" type="submit" class="btn btn-danger">Delete</a>
            </td>
        </tr>
    @endforeach
  </tbody>
</table>

</div>


@endsection