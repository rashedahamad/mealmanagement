@extends('layouts.MemberProfileModification')

@section('content')


<div class="container">

<form>

  <div class="form-group">
    <label for="exampleInputEmail1">User Name</label>
    <input type="text" readonly="true" class="form-control" value="{{ $student->user_name }}">  
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="text" readonly="true" class="form-control" value="{{ $student->email }}">  
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Gender</label>
    <input type="text" readonly="true" class="form-control" value="{{ $student->gender }}">  
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Amount</label>
    <input type="text" readonly="true" class="form-control" value="{{ $student->amount }}">  
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Number of meal</label>
    <input type="text" readonly="true" class="form-control" value="{{ $student->meal }}">  
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Pone</label>
    <input type="text" readonly="true" class="form-control" value="{{ $student->phone }}">  
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Date of Birth</label>
    <input type="text" readonly="true" class="form-control" value="{{ $student->dob }}">  
  </div>


  
  
</form>


</div>

@endsection