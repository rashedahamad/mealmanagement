@extends('layouts.admin-app')

@section('content')

<div class="container">

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Day</th>
      <th scope="col">Item 1</th>
      <th scope="col">Item 2</th>
    </tr>
  </thead>
  <tbody>
      @foreach($foodmenus as $foodmenu)
        <tr>
            <td> {{ $foodmenu -> id }} </td>
            <td> @if($foodmenu -> id == 1)
                    Saturday
                 @elseif($foodmenu -> id == 2)
                    Sunday
                    @elseif($foodmenu -> id == 3)
                    Monday
                    @elseif($foodmenu -> id == 4)
                    Tuesday
                    @elseif($foodmenu -> id == 5)
                    Wednesday
                    @elseif($foodmenu -> id == 6)
                    Thursday
                    @elseif($foodmenu -> id == 7)
                    Friday
                 @endif
            </td>
            <td> {{ $foodmenu -> item1 }} </td>
            <td> {{ $foodmenu -> item2 }} </td>
        
   
  
 
            
            </td>
            

            <td>
                <a href="{{route('admin.editfood', $foodmenu->id)}}" type="submit" class="btn btn-primary">Edit</a>

                
            </td>
        </tr>
         @endforeach
  
  </tbody>
</table>


</div>

</div>


@endsection