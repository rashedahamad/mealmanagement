@if(Session::get('user_role') == '1')

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('/public/js/app.js') }}" defer></script>

    <!-- Fonts -->
    
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('/public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/public/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Meal System</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a style="padding-left:20px;" class="nav-link" href="{{ route('admin.home') }}">Home <span class="sr-only">(current)</span></a>
      </li>
      
      <li class="nav-item active">
        <a style="padding-left:20px;"class="nav-link" href="{{ route('admin.foodmenu') }}"> Food Menu <span class="sr-only">(current)</span></a>
      </li><li class="nav-item active">
        <a style="padding-left:20px;"class="nav-link" href="{{ route('member.modification') }}">All User Info <span class="sr-only">(current)</span></a>
      </li><li class="nav-item active">
        <a style="padding-left:20px;"class="nav-link" href="{{ route('reset') }}">Reset <span class="sr-only">(current)</span></a>
      </li><li class="nav-item active">
        <a style="padding-left:20px;"class="nav-link" href="{{ route('admin.mealrate') }}"> Meal Rate <span class="sr-only">(current)</span></a>
      </li><li class="nav-item active">
        <a style="padding-left:20px;"class="nav-link" href="{{ route('showaddamount') }}"> Add Amount <span class="sr-only">(current)</span></a>
      </li><li class="nav-item active">
        <a style="padding-left:20px;"class="nav-link" href="{{ route('admin.showprofile') }}">Profile<span class="sr-only">(current)</span></a>
      </li>
      </li>
      </li><li class="nav-item active">
        <a style="padding-left:20px;" class="nav-link" href="{{ route('user.signout') }}"> Logout <span class="sr-only">(current)</span></a>
      </li>

      <li  class="nav-item active">
        <a style="padding-left:150px;" class="navbar-brand" href="#">{{ Session::get('user_name') }}<span class="sr-only">(current)</span></a>
      </li>
    </ul>
    
  </div>
</nav>

    @yield('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{ asset('/public/css/bootstrap.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>

@else
<script>

  window.location.href = '{{route("login")}}'; 


</script>

@endif