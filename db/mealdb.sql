-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 06, 2019 at 06:27 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mealdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `foodmenus`
--

DROP TABLE IF EXISTS `foodmenus`;
CREATE TABLE IF NOT EXISTS `foodmenus` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `foodmenus`
--

INSERT INTO `foodmenus` (`id`, `item1`, `item2`, `created_at`, `updated_at`) VALUES
(1, 'Kachchi Biryani\r\n', 'Egg', NULL, NULL),
(2, 'Bhuna Khichuri\r\n', 'Vegetable', NULL, NULL),
(3, 'Patla Khichuri\r\n', 'Egg', NULL, NULL),
(4, 'Morog Polao\r\n', 'Vegetable', NULL, NULL),
(5, 'Tehari\r\n', 'Egg', NULL, NULL),
(6, 'Beef Biryani\r\n', 'Vegetable Curry', NULL, NULL),
(7, 'Haji Biryani', 'egg', NULL, '2019-04-29 01:05:27');

-- --------------------------------------------------------

--
-- Table structure for table `foods`
--

DROP TABLE IF EXISTS `foods`;
CREATE TABLE IF NOT EXISTS `foods` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `day` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `foods`
--

INSERT INTO `foods` (`id`, `day`, `item`, `created_at`, `updated_at`) VALUES
(1, 'Saturday', 'vegetable', NULL, '2019-07-08 05:56:35'),
(2, 'Sunday', 'Fish', NULL, '2019-07-09 12:31:07'),
(3, 'Monday', 'Rice', NULL, '2019-07-08 04:20:22'),
(12, 'Tuesday', 'Rice', '2019-07-08 05:55:55', '2019-07-08 05:56:09'),
(11, 'Wednesday', 'Egg', '2019-07-08 05:55:40', '2019-07-08 05:56:21'),
(13, 'Thursday', 'Rice', '2019-07-08 05:57:05', '2019-07-08 05:57:05'),
(14, 'Friday', 'Beef', '2019-07-08 05:57:25', '2019-07-08 05:57:25'),
(15, 'Friday', 'fish', '2019-07-12 03:23:25', '2019-07-12 03:23:50');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2019_04_19_173504_create_students_table', 1),
(11, '2019_04_26_184704_create_foodmenus_table', 1),
(12, '2019_07_07_172121_create_foods_table', 2),
(13, '2019_07_07_172623_add_day_to_foods', 3),
(14, '2019_07_07_173217_add_item_to_foods', 4);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
CREATE TABLE IF NOT EXISTS `students` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meal` int(11) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `user_name`, `first_name`, `last_name`, `email`, `dob`, `phone`, `gender`, `password`, `meal`, `amount`, `image`, `role`, `created_at`, `updated_at`) VALUES
(1, 'redwan', 'redwan', 'ahamad', 'redwan.ahamad@gmail.com', '31-05-1997', '01686095009', 'male', '7445', 0, 0, NULL, 1, NULL, '2019-04-28 03:18:08'),
(2, 'siddik', 'siddik', 'ahamad', 'siddik.ahamad506@gmail.com', '03/03/1996', '01686095009', 'male', '7445', 5, 130, 'IMG_20170907_170430.jpg', 0, '2019-04-27 16:02:22', '2019-08-06 00:04:20'),
(4, 'rehana', 'rehana', 'akter', 'rehana.akter506@gmail.com', '03/02/1995', '01686095009', 'female', '7445', 5, 150, NULL, 0, '2019-04-28 02:47:21', '2019-08-06 00:00:03'),
(5, 'zaman', 'zaman', 'rashiduz', 'rashiduz.zaman506@gmail.com', '5/04/1974', '01686095009', 'male', '7445', 5, 120, 'IMG_20170907_100401.jpg', 0, '2019-04-28 03:14:43', '2019-08-06 00:20:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
