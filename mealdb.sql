-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2019 at 06:11 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mealdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `foodmenus`
--

CREATE TABLE `foodmenus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `foodmenus`
--

INSERT INTO `foodmenus` (`id`, `item1`, `item2`, `created_at`, `updated_at`) VALUES
(1, 'Kachchi Biryani\r\n', 'Egg', NULL, NULL),
(2, 'Bhuna Khichuri\r\n', 'Vegetable', NULL, NULL),
(3, 'Patla Khichuri\r\n', 'Egg', NULL, NULL),
(4, 'Morog Polao\r\n', 'Vegetable', NULL, NULL),
(5, 'Tehari\r\n', 'Egg', NULL, NULL),
(6, 'Beef Biryani\r\n', 'Vegetable Curry', NULL, NULL),
(7, 'Haji Biryani', 'egg', NULL, '2019-04-29 01:05:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(10, '2019_04_19_173504_create_students_table', 1),
(11, '2019_04_26_184704_create_foodmenus_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meal` int(11) NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `user_name`, `first_name`, `last_name`, `email`, `dob`, `phone`, `gender`, `password`, `meal`, `amount`, `image`, `role`, `created_at`, `updated_at`) VALUES
(1, 'redwan', 'redwan', 'ahamad', 'redwan.ahamad@gmail.com', '31-05-1997', '01686095009', 'male', '7445', 0, 0, NULL, 1, NULL, '2019-04-28 03:18:08'),
(2, 'siddik', 'siddik', 'ahamad', 'siddik.ahamad506@gmail.com', '03/03/1996', '01686095009', 'male', '7445', 5, 130, NULL, 0, '2019-04-27 16:02:22', '2019-04-29 01:20:28'),
(4, 'rehana', 'rehana', 'akter', 'rehana.akter506@gmail.com', '03/02/1995', '01686095009', 'female', '7445', 5, 100, NULL, 0, '2019-04-28 02:47:21', '2019-04-28 03:32:31'),
(5, 'zaman', 'zaman', 'rashiduz', 'rashiduz.zaman506@gmail.com', '5/04/1974', '01686095009', 'male', '7445', 5, 120, NULL, 0, '2019-04-28 03:14:43', '2019-04-29 01:20:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `foodmenus`
--
ALTER TABLE `foodmenus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `foodmenus`
--
ALTER TABLE `foodmenus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
